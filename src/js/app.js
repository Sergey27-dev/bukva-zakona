import * as flsFunctions from "./modules/functions.js";

import jQuery from "jquery";
window.$ = window.jQuery = jQuery;

flsFunctions.isWebp();

$('document').ready(function() {
    $(".header-burger").click(function (e) {
        $(".header-burger, nav").toggleClass('active');
    })

    $(document).click(function (e) {
        if (!$("nav").is(e.target) && !$(".header-burger").is(e.target) && !$("nav ul").is(e.target)) {
          if($("nav").hasClass("active"))
          $(".header-burger, nav").toggleClass('active');
        }
      });


    $("a").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();
          var hash = this.hash;
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
            window.location.hash = hash;
          });
        }
      });
})